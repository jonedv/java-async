package se.cygni.async.ex5;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import se.cygni.async.Exercise;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;

import static org.slf4j.LoggerFactory.getLogger;

@Component("sync")
public class SyncServlet extends HttpServlet {

    private static final Logger LOGGER = getLogger(SyncServlet.class);

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        try {
            long start = System.currentTimeMillis();
            Thread.sleep(new Random().nextInt(2000));
            String name = Thread.currentThread().getName();
            long duration = System.currentTimeMillis() - start;
            response.getWriter().printf("Thread %s completed the task in %d ms.", name, duration);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
