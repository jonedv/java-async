package se.cygni.async;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
//@Target({ElementType.METHOD})
public @interface Exercise {
}
