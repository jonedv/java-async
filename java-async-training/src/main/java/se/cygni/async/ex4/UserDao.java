package se.cygni.async.ex4;

import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class UserDao {
    private Map<String, User> map = new ConcurrentHashMap<>();

    public UserDao() {
        save(new User("jon"));
        save(new User("tommy"));
        save(new User("annika"));
        save(new User("per"));
    }

    public Optional<User> findUser(String name) {
        return Optional.ofNullable(map.get(name));
    }

    public void save(User user) {
        map.put(user.getName(), user);
    }

}
