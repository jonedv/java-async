package se.cygni.async.ex1;

public class PrimeService {
    public boolean isPrime(long number) {
        if (number < 2) {
            return false;
        }
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Computes the first prime greater than <code>minValue</code>.
     *
     * @param minValue
     * @return a prime
     */
    public long first(long minValue) {
        long i = minValue;
        while (i < Long.MAX_VALUE && !isPrime(i)) {
            i++;
        }
        return i;
    }
}
