package se.cygni.async.ex2;

import org.junit.Test;
import se.cygni.async.Exercise;

import java.util.concurrent.TimeoutException;

public class WaitNotifyTest {

    @Exercise
    @Test(expected = TimeoutException.class)
    public void testExecuteAsyncTimeout() throws Exception {
        Integer integer = WaitNotify.executeAsync(() -> {
            Thread.sleep(10000);
            return 7;
        }, 1000);
    }


    @Exercise
    @Test
    public void testExecuteAsyncInterrupted() throws Exception {
    }

}