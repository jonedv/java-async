package se.cygni.async.threads;

public class BankAccountTransfer {
    private static class Account {
        private final String name;
        private int total;

        public Account(String name, int total) {
            this.name = name;
            this.total = total;
        }

        public synchronized void deposit(int amount) {
            requirePositive(amount);
            total += amount;
        }

        public synchronized int withdraw(int amount) {
            requirePositive(amount);
            amount = Math.min(amount, total - amount);
            total -= amount;
            return amount;
        }

        public synchronized int transfer(Account to, int amount) {
            amount = withdraw(amount);
            to.deposit(amount);
            return amount;
        }

        private void requirePositive(int amount) {
            if (amount <= 0) {
                throw new IllegalArgumentException("amount < 0: " + amount);
            }
        }

        @Override
        public synchronized String toString() {
            return name + ": " + total;
        }
    }


    private static class Transfer extends Thread {

        private final Account from;
        private final Account to;
        private final int amount;

        public Transfer(Account from, Account to, int amount) {
            this.from = from;
            this.to = to;
            this.amount = amount;
        }

        public void run() {
            from.transfer(to, amount);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Account sallaryAccount = new Account("sallery", 1000);
        Account savingsAccount = new Account("savings", 1000);
        for (int i = 0; i < 1000; i++) {
            new Transfer(sallaryAccount, savingsAccount, 200).start();
            new Transfer(savingsAccount, sallaryAccount, 200).start();
        }
        System.out.println(sallaryAccount);
        System.out.println(savingsAccount);
    }

}