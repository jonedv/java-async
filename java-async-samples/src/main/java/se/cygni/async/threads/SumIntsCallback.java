package se.cygni.async.threads;

import java.util.Random;
import java.util.function.LongConsumer;

public class SumIntsCallback {
    private static Object LOCK = new Object();
    private static boolean done;
    private static long total;

    private static class SumThread extends Thread {
        private final LongConsumer consumer;

        public SumThread(LongConsumer consumer) {
            this.consumer = consumer;
        }

        public void run() {
            Random random = new Random();
            long total = random.ints(100000000).sum();
            System.out.println("sum() finished: " + total);

            consumer.accept(total);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        String message = "Got total: ";
        new SumThread(total -> System.out.println(message + total)).start();
        System.out.println("Main thread exiting...");
    }
}
